<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\User
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $iduser;

    /**
     * @ORM\Column(name="`name`", type="string", length=45)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="user")
     * @ORM\JoinColumn(name="iduser", referencedColumnName="user_iduser", nullable=false, onDelete="CASCADE")
     */
    protected $articles;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="user")
     * @ORM\JoinColumn(name="iduser", referencedColumnName="user_iduser", nullable=false, onDelete="CASCADE")
     */
    protected $comments;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Set the value of iduser.
     *
     * @param integer $iduser
     * @return \App\Entity\User
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;

        return $this;
    }

    /**
     * Get the value of iduser.
     *
     * @return integer
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * Set the value of name.
     *
     * @param string $name
     * @return \App\Entity\User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add Article entity to collection (one to many).
     *
     * @param \App\Entity\Article $article
     * @return \App\Entity\User
     */
    public function addArticle(Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove Article entity from collection (one to many).
     *
     * @param \App\Entity\Article $article
     * @return \App\Entity\User
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);

        return $this;
    }

    /**
     * Get Article entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add Comment entity to collection (one to many).
     *
     * @param \App\Entity\Comment $comment
     * @return \App\Entity\User
     */
    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove Comment entity from collection (one to many).
     *
     * @param \App\Entity\Comment $comment
     * @return \App\Entity\User
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    /**
     * Get Comment entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function __sleep()
    {
        return array('iduser', 'name');
    }
}