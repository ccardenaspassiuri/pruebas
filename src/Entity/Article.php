<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * App\Entity\Article
 *
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\Table(name="article", indexes={@ORM\Index(name="fk_article_user1_idx", columns={"user_iduser"})})
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idarticle;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @ORM\Column(type="integer")
     */
    protected $user_iduser;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article")
     * @ORM\JoinColumn(name="idarticle", referencedColumnName="article_idarticle", nullable=false, onDelete="CASCADE")
     */
    protected $comments;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
     * @ORM\JoinColumn(name="user_iduser", referencedColumnName="iduser", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    /**
     * Set the value of idarticle.
     *
     * @param integer $idarticle
     * @return \App\Entity\Article
     */
    public function setIdarticle($idarticle)
    {
        $this->idarticle = $idarticle;

        return $this;
    }

    /**
     * Get the value of idarticle.
     *
     * @return integer
     */
    public function getIdarticle()
    {
        return $this->idarticle;
    }

    /**
     * Set the value of title.
     *
     * @param string $title
     * @return \App\Entity\Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of content.
     *
     * @param string $content
     * @return \App\Entity\Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of user_iduser.
     *
     * @param integer $user_iduser
     * @return \App\Entity\Article
     */
    public function setUserIduser($user_iduser)
    {
        $this->user_iduser = $user_iduser;

        return $this;
    }

    /**
     * Get the value of user_iduser.
     *
     * @return integer
     */
    public function getUserIduser()
    {
        return $this->user_iduser;
    }

    /**
     * Add Comment entity to collection (one to many).
     *
     * @param \App\Entity\Comment $comment
     * @return \App\Entity\Article
     */
    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove Comment entity from collection (one to many).
     *
     * @param \App\Entity\Comment $comment
     * @return \App\Entity\Article
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    /**
     * Get Comment entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set User entity (many to one).
     *
     * @param \App\Entity\User $user
     * @return \App\Entity\Article
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User entity (many to one).
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __sleep()
    {
        return array('idarticle', 'title', 'content', 'user_iduser');
    }
}