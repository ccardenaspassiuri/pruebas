<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App\Entity\Comment
 *
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @ORM\Table(name="`comment`", indexes={@ORM\Index(name="fk_comment_user1_idx", columns={"user_iduser"}), @ORM\Index(name="fk_comment_article1_idx", columns={"article_idarticle"})})
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idcomment;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="integer")
     */
    protected $user_iduser;

    /**
     * @ORM\Column(type="integer")
     */
    protected $article_idarticle;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="user_iduser", referencedColumnName="iduser", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="article_idarticle", referencedColumnName="idarticle", nullable=false, onDelete="CASCADE")
     */
    protected $article;

    public function __construct()
    {
    }

    /**
     * Set the value of idcomment.
     *
     * @param integer $idcomment
     * @return \App\Entity\Comment
     */
    public function setIdcomment($idcomment)
    {
        $this->idcomment = $idcomment;

        return $this;
    }

    /**
     * Get the value of idcomment.
     *
     * @return integer
     */
    public function getIdcomment()
    {
        return $this->idcomment;
    }

    /**
     * Set the value of description.
     *
     * @param string $description
     * @return \App\Entity\Comment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of user_iduser.
     *
     * @param integer $user_iduser
     * @return \App\Entity\Comment
     */
    public function setUserIduser($user_iduser)
    {
        $this->user_iduser = $user_iduser;

        return $this;
    }

    /**
     * Get the value of user_iduser.
     *
     * @return integer
     */
    public function getUserIduser()
    {
        return $this->user_iduser;
    }

    /**
     * Set the value of article_idarticle.
     *
     * @param integer $article_idarticle
     * @return \App\Entity\Comment
     */
    public function setArticleIdarticle($article_idarticle)
    {
        $this->article_idarticle = $article_idarticle;

        return $this;
    }

    /**
     * Get the value of article_idarticle.
     *
     * @return integer
     */
    public function getArticleIdarticle()
    {
        return $this->article_idarticle;
    }

    /**
     * Set User entity (many to one).
     *
     * @param \App\Entity\User $user
     * @return \App\Entity\Comment
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User entity (many to one).
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set Article entity (many to one).
     *
     * @param \App\Entity\Article $article
     * @return \App\Entity\Comment
     */
    public function setArticle(Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get Article entity (many to one).
     *
     * @return \App\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    public function __sleep()
    {
        return array('idcomment', 'description', 'user_iduser', 'article_idarticle');
    }
}