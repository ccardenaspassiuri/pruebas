<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class UserController
{
    private $user;

    public function __construct(UserService $user)
    {
        $this->user = $user;
    }


    /**
     * @Route("/user", name="user")
     */
    public function post(Request $request): JsonResponse
    {
        $response = $this->user->postUser($request);
        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData([
                'id' => $response->getIdUser(),
                'name'=>$response->getName()]);
    }


    /**
     * @Route("/users", name="users")
     */
    public function getAll()
    {

        $response= $this->user->getAllUsers();

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }


    /**
     * @Route("/users/{iduser}", name="users_{iduser}")
     */
    public function getById(Request $request)
    {

        $response= $this->user->getById($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }


    /**
     * @Route("/deleteusers/{iduser}", name="deleteusers_{iduser}")
    */
    public function deleteById(Request $request)
    {

        $response= $this->user->deleteUser($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }



    /**
     * @Route("/putuser/{iduser}", name="putuser_{iduser}")
     */
    public function putById(Request $request)
    {

        $response= $this->user->putUser($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }



}