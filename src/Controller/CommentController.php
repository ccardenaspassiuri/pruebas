<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Service\CommentService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class CommentController
{
    private $comment;

    public function __construct(CommentService $comment)
    {
        $this->comment = $comment;
    }


    /**
     * @Route("/comment", name="comment")
     */
    public function post(Request $request): JsonResponse
    {
        $response = $this->comment->postComment($request);
        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData([
                'id' => $response->getIdcomment(),
                'description'=> $response->getDescription(),
                'idArticle'=> $response->getArticle()->getIdarticle(),
                'idUser'=> $response->getUser()->getIduser()
            ]);
    }


    /**
     * @Route("/comments", name="comments")
     */
    public function getAll()
    {

        $response= $this->comment->getAllComments();

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }


    /**
     * @Route("/comments/{user_iduser}", name="comments_{user_iduser}")
     */
    public function getByUser(Request $request)
    {

        $response= $this->comment->getCommentsByUser($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }


    /**
     * @Route("/comments/{user_iduser}/{idcomment}", name="comments_{user_iduser}_{idcomment}")
     */
    public function getById(Request $request)
    {

        $response= $this->comment->getCommentById($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
        //return (new JsonResponse())->setStatusCode(201)->setData(['id'=>$response->getIdArticle()]);
    }



    /**
     * @Route("/deletecomments/{idcomment}", name="deletecomments_{idcomment}")
     */
    public function deleteComment(Request $request)
    {

        $response= $this->comment->deleteComment($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
        //return (new JsonResponse())->setStatusCode(201)->setData(['id'=>$response->getIdArticle()]);
    }


    /**
     * @Route("/putcomment/{idcomment}", name="putcomment_{idcomment}")
     */
    public function putComment(Request $request)
    {

        $response= $this->comment->putComment($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
        //return (new JsonResponse())->setStatusCode(201)->setData(['id'=>$response->getIdArticle()]);
    }


}