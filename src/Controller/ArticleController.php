<?php

namespace App\Controller;

use App\Entity\Article;
use App\Service\ArticleService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class ArticleController
{
    private $article;

    public function __construct(ArticleService $article)
    {
        $this->article = $article;
    }


    /**
     * @Route("/article", name="article")
     */
    public function post(Request $request): JsonResponse
    {
        $response = $this->article->postArticle($request);
        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData([
                'id' => $response->getIdarticle(),
                'title'=> $response->getTitle(),
                'content'=> $response->getContent(),
                'idUser'=> $response->getUser()->getIduser()
        ]);
    }


    /**
     * @Route("/articles", name="articles")
     */
    public function getAll()
    {

        $response= $this->article->getAllArticles();

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }



    /**
     * @Route("/articles/{user_iduser}", name="articles_{user_iduser}")
     */
    public function getByUser(Request $request)
    {

        $response= $this->article->getArticlesByUser($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }


    /**
     * @Route("/articles/{user_iduser}/{idarticle}", name="articles_{user_iduser}_{idarticle}")
     */
    public function getById(Request $request)
    {

        $response= $this->article->getArticlesById($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
            //return (new JsonResponse())->setStatusCode(201)->setData(['id'=>$response->getIdArticle()]);
    }


    /**
     * @Route("/deletearticles/{idarticle}", name="deletearticles_{idarticle}")
     */
    public function deleteById(Request $request)
    {

        $response= $this->article->deleteArticle($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }


    /**
     * @Route("/putarticle/{idarticle}", name="putarticle_{idarticle}")
     */
    public function putById(Request $request)
    {

        $response= $this->article->putArticle($request);

        if (!$response)
            return (new JsonResponse())->setStatusCode(500);
        else
            return (new JsonResponse())->setStatusCode(200)->setData($response);
    }


}