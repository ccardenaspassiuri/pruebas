<?php

namespace App\Service;
use App\Entity\Article;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Comment;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;




class CommentService
{
    protected $entityManager;
    /**
     * CommentService constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Create Comment.
     * @FOSRest\Post("/comment")
     *

     */
    public function postComment(Request $request): ?Comment
    {
        $user_iduser = $request->get('user_iduser');
        //var_dump($user_iduser);exit();
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['iduser' => $user_iduser]);
        $article_idarticle = $request->get('article_idarticle');
       // var_dump($article_idarticle);exit();
        $article = $this->entityManager->getRepository(Article::class)->findOneBy(['idarticle' => $article_idarticle]);
        if(!$user && !$article) {
            return null;
        }
        $comment = new Comment();
        $comment->setDescription($request->get('description'));
        $comment->setUser($user);
        $comment->setArticle($article);
        try {
            $this->entityManager->persist($comment);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            var_dump($e->getMessage());exit();
        }
        return $comment;
    }




    /**
     * Lists all Comments.
     * @FOSRest\Get("/comments")
     *
     * @return array
     */
    public function getAllComments()
    {
        $array = array();
        try {
            $comments = $this->entityManager->getRepository(Comment::class)->findAll();
            //var_dump($comments);exit();
            foreach ($comments as $item) {
                $array[] = array(
                    'idcomment'=>$item->getIdcomment(),
                    'user_iduser'=>$item->getUserIduser(),
                    'article_idarticle'=>$item->getArticleIdarticle(),
                    'description'=>$item->getDescription()
                );
            }
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }

        return $array;
    }



    /**
     * Lists comments by user.
     * @FOSRest\Get("/comments/{user_iduser}")
     *
     * @return array
     */
    public function getCommentsByUser(Request $request) :bool
    {
        $array = array();
        try {
            $user_iduser = $request->get('user_iduser');
            $comments = $this->entityManager->getRepository(Comment::class)->findBy(['user_iduser' => $user_iduser]);
            //var_dump($articles);exit();
            foreach ($comments as $item) {
                $array[] = array(
                    'idcomment'=>$item->getIdcomment(),
                    'user_iduser'=>$item->getUserIduser(),
                    'article_idarticle'=>$item->getArticleIdarticle(),
                    'description'=>$item->getDescription()
                );
            }
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
            return 0;
        }

        return 1;
    }


    /**
     * Lists articles by id.
     * @FOSRest\Get("/comments/{user_iduser}/{idcomment}")
     *
     * @return array
     */
    public function getCommentById(Request $request)
    {
        $array = array();
        try {
            $user_iduser = $request->get('user_iduser');
            $comment = $this->entityManager->getRepository(Comment::class)->findOneBy([
                'user_iduser' => $user_iduser,
                'idcomment' => $request->get('idcomment')]);
            //var_dump($articles);exit();

            $array = array(
                'idcomment'=>$comment->getIdcomment(),
                'user_iduser'=>$comment->getUserIduser(),
                'article_idarticle'=>$comment->getArticleIdarticle(),
                'description'=>$comment->getDescription()
            );

        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }

        return $array;
    }


    /**
     * Delete Comments.
     * @FOSRest\Delete("/deletecomments/{idcomment}")
     *
     * @return array
     **/
    public function deleteComment(Request $request)
    {
        try {
            $comment = $this->entityManager->getRepository(Comment::class)->findOneBy(['idcomment' => $request->get('idcomment')]);

            $this->entityManager->remove($comment);
            $this->entityManager->flush();
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }
        return('deleted successfully');
    }


    /**
     * Delete Comments.
     * @FOSRest\Put("/putcomment/{idcomment}")
     *
     **/
    public function putComment(Request $request)
    {
        try {
            $comment = $this->entityManager->getRepository(Comment::class)->findOneBy(['idcomment' => $request->get('idcomment')]);

            if ($request->get('description'))
                $comment->setDescription($request->get('description'));
            $this->entityManager->persist($comment);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            var_dump($e->getMessage());exit();
        }
        return $comment;
    }


}