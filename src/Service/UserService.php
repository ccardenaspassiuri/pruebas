<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use App\Service\ArticleService;
use App\Service\CommentService;


class UserService
{
    protected $entityManager;
    /**
     * UserService constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Create User.
     * @FOSRest\Post("/user")
     *

     */
    public function postUser(Request $request): ?User
    {
        $user = new User();
        $user->setName($request->get('name'));
        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            return null;
        }
        return $user;
    }




    /**
     * Lists all Users.
     * @FOSRest\Get("/users")
     *
     * @return array
     */
    public function getAllUsers()
    {
        $user = new User();
        $array = array();
        try {
            $users = $this->entityManager->getRepository(User::class)->findAll();
            foreach ($users as $item) {
                    $array[] = array(
                        'iduser'=>$item->getIduser(),
                        'name'=>$item->getName()
                    );
            }
        }
        catch (\Exception $e) {
            return null;
        }

        return $array;
    }


    /**
     *
     * @FOSRest\Get("/users/{iduser}")
     *
     * @return array
     */
    public function getById(Request $request)
    {
        try {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['iduser' => $request->get('iduser')]);

            $array = array (
                'iduser'=>$user->getIduser(),
                'name'=>$user->getName()
            );
        }
        catch (\Exception $e) {
            return null;
        }

        return $array;
    }



    /**
     * Delete Users.
     * @FOSRest\Delete("/deleteusers/{iduser}")
     *
     * @return array
    **/
    public function deleteUser(Request $request)
    {
        try {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['iduser' => $request->get('iduser')]);
            $this->entityManager->remove($user);
            $this->entityManager->flush();
        }
        catch (\Exception $e) {
            return null;
            //var_dump($e->getMessage());
            //exit();
        }
        return('deleted successfully');
    }



    /**
     * Put Users.
     * @FOSRest\Put("/putuser/{iduser}")
     *
     * @return array
     **/
    public function putUser(Request $request) :User
    {
        //$array = array();
        try {
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['iduser' => $request->get('iduser')]);

            $user->setName($request->get('name'));
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }
        return $user;
    }

}