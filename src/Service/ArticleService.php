<?php

namespace App\Service;

use App\Entity\Article;
use App\Entity\User;
use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ArticleService
{

    protected $entityManager;
    /**
     * ArticleService constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Create Article.
     * @FOSRest\Post("/article")
     *

     */
    public function postArticle(Request $request) :?Article
    {
        $user_iduser = $request->get('user_iduser');
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['iduser' => $user_iduser]);

        $article = new Article();
        $article->setTitle($request->get('title'));
        $article->setContent($request->get('content'));
        $article->setUser($user);
        // $article->setUserIduser($request->get('user_iduser'));
        try {
            $this->entityManager->persist($article);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            var_dump($e->getMessage());exit();
        }
        return $article;
    }




    /**
     * Lists all Articles.
     * @FOSRest\Get("/articles")
     *
     * @return array
     */
    public function getAllArticles()
    {
        $array = array();
        try {
            $articles = $this->entityManager->getRepository(Article::class)->findAll();
            foreach ($articles as $item) {
                $array[] = array(
                    'idarticle'=>$item->getIdarticle(),
                    'user_iduser'=>$item->getUserIduser(),
                    'title'=>$item->getTitle(),
                    'content'=>$item->getContent()
                );
            }
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }

        return $array;
    }



    /**
     * Lists articles by user.
     * @FOSRest\Get("/articles/{user_iduser}")
     *
     * @return array
     */
    public function getArticlesByUser(Request $request) :bool
    {
        $array = array();
        try {
            $user_iduser = $request->get('user_iduser');
            $articles = $this->entityManager->getRepository(Article::class)->findBy(['user_iduser' => $user_iduser]);
            //var_dump($articles);exit();
            foreach ($articles as $item) {
                $array[] = array(
                    'idarticle'=>$item->getIdarticle(),
                    'user_iduser'=>$item->getUserIduser(),
                    'title'=>$item->getTitle(),
                    'content'=>$item->getContent()
                );
            }
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
            return 0;
        }

        //return $array;
        return 1;
    }



    /**
     * Lists articles by id.
     * @FOSRest\Get("/articles/{user_iduser}/{idarticle}")
     *
     * @return array
     */
    public function getArticlesById(Request $request)
    {
        $array = array();
        try {
            $user_iduser = $request->get('user_iduser');
            $article = $this->entityManager->getRepository(Article::class)->findOneBy([
                'user_iduser' => $user_iduser,
                'idarticle' => $request->get('idarticle')]);
            //var_dump($articles);exit();

                $array = array(
                    'idarticle'=>$article->getIdarticle(),
                    'user_iduser'=>$article->getUserIduser(),
                    'title'=>$article->getTitle(),
                    'content'=>$article->getContent()
                );

        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }

        return $array;
    }



    /**
     * Delete Articles.
     * @FOSRest\Delete("/deletearticles/{idarticle}")
     *
     * @return array
     **/
    public function deleteArticle(Request $request)
    {
        try {
            $article = $this->entityManager->getRepository(Article::class)->findOneBy(['idarticle' => $request->get('idarticle')]);

            $this->entityManager->remove($article);
            $this->entityManager->flush();
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }
        return('deleted successfully');
    }


    /**
     * update Articles.
     * @FOSRest\Patch("/putarticle/{idarticle}")
     *
     * @return array
     **/
    public function putArticle(Request $request) :Article
    {

        $article = $this->entityManager->getRepository(Article::class)->findOneBy(['idarticle' => $request->get('idarticle')]);

        try {
            if ($request->get('title'))
                $article->setTitle($request->get('title'));
            if ($request->get('content'))
                $article->setContent($request->get('content'));
            //$article->setUser($request->get('user_iduser'));
            if ($request->get('user_iduser'))
                $article->setUserIduser($request->get('user_iduser'));
            $this->entityManager->persist($article);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            exit();
        }
        return $article;
    }
}