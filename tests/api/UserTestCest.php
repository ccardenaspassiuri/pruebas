<?php
namespace App\Tests;
use App\Tests\ApiTester;

class UserTestCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
    }

    public function _after(ApiTester $I)
    {
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    // tests
    public function postUser(ApiTester $I)
    {
        $I->wantTo('test user post method');
        $I->sendPOST('/user', '{
            "name": "usuarioTest"
             }');
    }


    public function getAllUsers(ApiTester $I)
    {
        $I->wantTo('test user get all method');
        $I->sendGET('/users');
    }


    public function getUserById(ApiTester $I)
    {
        $I->wantTo('test user get all method');
        $I->sendGET('/users/2');
    }


    public function deleteUser(ApiTester $I)
    {
        $I->wantTo('test user delete method');
        $I->sendDELETE('/deleteusers/10');
    }


    public function putUser(ApiTester $I)
    {
        $I->wantTo('test user put method');
        $I->sendPUT('/putuser/8', '{ "name": "newUser" }');
    }

}
